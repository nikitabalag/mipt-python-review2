import collections
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

MARKS = collections.defaultdict(list)
token = ''
ADRESS = []
TRAVELMODE = 'transit'  # по умолчанию катаемся на общественном транспорте


def start(bot, update):
    update.message.reply_text('Hi, @{}!'.
                              format(update.effective_user.username))


def echo(bot, update):
    update.message.reply_text(update.message.text)


def adress(bot, update, args):
    global ADRESS
    ADRESS.append(args[0])
    update.message.reply_text('added ' + args[0])


def show_history(bot, update):
    if update.effective_user.username == 'nikitabalagansky':
        for i in ADRESS:
            update.message.reply_text(str(i))
    else:
        update.message.reply_text('You can\'t view history')

def setTravelMode(bot, update, args):


        


def main():
    token = open('constants.txt').read()
    updater = Updater(token)
    dp = updater.dispatcher

    # команды
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("adress", adress, pass_args=True))
    dp.add_handler(CommandHandler("showHistory", show_history))
    dp.add_handler(CommandHandler('setTravelMode'), setTravelMode, pass_args=True)
    # # Эхо
    dp.add_handler(MessageHandler(Filters.text, echo))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
